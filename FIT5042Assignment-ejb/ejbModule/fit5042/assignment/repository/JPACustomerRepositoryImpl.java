package fit5042.assignment.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import fit5042.assignment.repository.entities.Customer;
import fit5042.assignment.repository.entities.CustomerContact;
@Stateless
public class JPACustomerRepositoryImpl implements CustomerRepository{

	@PersistenceContext(unitName = "FIT5042Assignment-ejbPU")
	private EntityManager entityManager;
	
	@Override
	public void addCustomer(Customer customer) throws Exception {
		List<Customer> customers = entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
		customer.setCustomerId(customers.get(0).getCustomerId() + 1);
		entityManager.persist(customer);
	}

	@Override
	public Customer searchCustomerById(int customerId) throws Exception {
		Customer customer = entityManager.find(Customer.class, customerId);
		return customer;
		
	}

	@Override
	public List<Customer> getAllCustomers() throws Exception {
		return entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
	}

	@Override
	public List<CustomerContact> getAllCustomerContact() throws Exception {
		
		return entityManager.createNamedQuery(CustomerContact.GET_ALL_QUERY_NAME).getResultList();
	}

	@Override
	public void removeCustomer(int customerId) throws Exception {
		Customer customer = this.searchCustomerById(customerId);
		if(customer != null) {
			entityManager.remove(customer);
		}
		
	}

	@Override
	public void editCustomer(Customer customer) throws Exception {
		try {
			entityManager.merge(customer);
		} catch (Exception ex)
		{
			
		}
	}

	@Override
	public List<Customer> searchCustomerByRate(double rate) throws Exception {
		 CriteriaBuilder builder = entityManager.getCriteriaBuilder();
	        CriteriaQuery query = builder.createQuery(Customer.class);
	        Root<Customer> c = query.from(Customer.class);
	        query.select(c).where(builder.equal(c.get("rate").as(Double.class), rate));
	        List<Customer> lc = entityManager.createQuery(query).getResultList();
	        return lc;
	}

}
