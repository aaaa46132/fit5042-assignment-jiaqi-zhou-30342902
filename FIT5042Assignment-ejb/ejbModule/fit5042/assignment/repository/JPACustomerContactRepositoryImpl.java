package fit5042.assignment.repository;

import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fit5042.assignment.repository.entities.Customer;
import fit5042.assignment.repository.entities.CustomerContact;
@Stateless
public class JPACustomerContactRepositoryImpl implements CustomerContactRepository{
	
	@PersistenceContext(unitName = "FIT5042Assignment-ejbPU")
	private EntityManager entityManager;

	@Override
	public void addCustomerContact(CustomerContact customerContact) throws Exception {
		List<CustomerContact> customerContacts = entityManager.createNamedQuery(CustomerContact.GET_ALL_QUERY_NAME).getResultList();
		customerContact.setCustomerContactId(customerContacts.get(0).getCustomerContactId() + 1);
		entityManager.persist(customerContact);
	}

	@Override
	public CustomerContact searchCustomerContactById(int customerContactId) throws Exception {
		CustomerContact customerContact = entityManager.find(CustomerContact.class, customerContactId);
		return customerContact;
	}

	@Override
	public List<CustomerContact> getAllCustomerContacts() throws Exception {
		return entityManager.createNamedQuery(CustomerContact.GET_ALL_QUERY_NAME).getResultList();
	}

	@Override
	public Set<CustomerContact> searchCustomerContactByCustomer(Customer customer) throws Exception {
		customer = entityManager.find(Customer.class, customer.getCustomerId());
		customer.getCustomerContacts().size();
		entityManager.refresh(customer);
		
		return customer.getCustomerContacts();
	}

	@Override
	public void removeCustomerContact(int customerContactId) throws Exception {
		CustomerContact customerContact = this.searchCustomerContactById(customerContactId);
		if(customerContact != null) {
			entityManager.remove(customerContact);
		}
	}

	@Override
	public void editCustomerContact(CustomerContact customerContact) throws Exception {
		try {
			entityManager.merge(customerContact);
		} catch (Exception ex) {
			
		}
		
	}
	

}
