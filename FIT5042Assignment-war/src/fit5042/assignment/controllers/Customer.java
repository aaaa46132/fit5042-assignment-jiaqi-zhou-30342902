package fit5042.assignment.controllers;

import fit5042.assignment.repository.entities.Address;
import fit5042.assignment.repository.entities.CustomerContact;
import fit5042.assignment.repository.entities.Users;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@RequestScoped
@Named(value = "customer")
public class Customer {
	private int customerId;
	private String industryType;
	private Date dob;
	private String name;
	private String gender;
	private Users users;
	private Address address;
	private double rate;
	private Set<CustomerContact> customerContacts; 
	
    private String streetNumber;
    private String streetAddress;
    private String suburb;
    private String postcode;
    private String state;
    private String country;
	
	private int id;
	private String username;
	private String password;
	
	private Set<fit5042.assignment.repository.entities.Customer> customers;
	public Customer() {
		this.customerContacts = new HashSet<>();
	}
	public Customer(int customerId, String industryType, Date dob, String name, String gender, Users users,
			Address address, double rate, Set<CustomerContact> customerContacts, String streetNumber,
			String streetAddress, String suburb, String postcode, String state, String country, int id, String username,
			String password, Set<fit5042.assignment.repository.entities.Customer> customers) {
		this.customerId = customerId;
		this.industryType = industryType;
		this.dob = dob;
		this.name = name;
		this.gender = gender;
		this.users = users;
		this.address = address;
		this.rate = rate;
		this.customerContacts = customerContacts;
		this.streetNumber = streetNumber;
		this.streetAddress = streetAddress;
		this.suburb = suburb;
		this.postcode = postcode;
		this.state = state;
		this.country = country;
		this.id = id;
		this.username = username;
		this.password = password;
		this.customers = customers;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public String getIndustryType() {
		return industryType;
	}
	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Users getUsers() {
		return users;
	}
	public void setUsers(Users users) {
		this.users = users;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public Set<CustomerContact> getCustomerContacts() {
		return customerContacts;
	}
	public void setCustomerContacts(Set<CustomerContact> customerContacts) {
		this.customerContacts = customerContacts;
	}
	public String getStreetNumber() {
		return streetNumber;
	}
	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}
	public String getStreetAddress() {
		return streetAddress;
	}
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}
	public String getSuburb() {
		return suburb;
	}
	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}
	public String getPostcode() {
		return postcode;
	}
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Set<fit5042.assignment.repository.entities.Customer> getCustomers() {
		return customers;
	}
	public void setCustomers(Set<fit5042.assignment.repository.entities.Customer> customers) {
		this.customers = customers;
	}
	
	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", industryType=" + industryType + ", dob=" + dob + ", name="
				+ name + ", gender=" + gender + ", users=" + users + ", address=" + address + ", rate=" + rate
				+ ", customerContacts=" + customerContacts + ", streetNumber=" + streetNumber + ", streetAddress="
				+ streetAddress + ", suburb=" + suburb + ", postcode=" + postcode + ", state=" + state + ", country="
				+ country + ", id=" + id + ", username=" + username + ", password=" + password + ", customers="
				+ customers + "]";
	}
	
	
}
