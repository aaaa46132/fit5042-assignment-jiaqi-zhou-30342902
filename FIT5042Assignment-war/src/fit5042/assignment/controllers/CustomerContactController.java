package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named(value = "customerContactController")
@RequestScoped
public class CustomerContactController {
	private int customerContactId;

	public int getCustomerContactId() {
		return customerContactId;
	}

	public void setCustomerContactId(int customerContactId) {
		this.customerContactId = customerContactId;
	}
	
	private fit5042.assignment.repository.entities.CustomerContact customerContact;
	
	public CustomerContactController() {
		customerContactId = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("customerContactId"));
		customerContact = getCustomerContact();
	}
	
	public fit5042.assignment.repository.entities.CustomerContact getCustomerContact(){
		if(customerContact == null)
		{
			 ELContext context
             = FacesContext.getCurrentInstance().getELContext();
			 CustomerContactApplication app
             = (CustomerContactApplication) FacesContext.getCurrentInstance()
                     .getApplication()
                     .getELResolver()
                     .getValue(context, null, "customerContactApplication");
			 return app.getCustomerContacts().get(--customerContactId);
		}
		return customerContact;
	}
}
