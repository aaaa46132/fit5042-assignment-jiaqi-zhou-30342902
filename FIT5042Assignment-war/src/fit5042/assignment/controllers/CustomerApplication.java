package fit5042.assignment.controllers;

import java.util.ArrayList;
import javax.enterprise.context.ApplicationScoped;
import fit5042.assignment.mbeans.CustomerManagedBean;
import javax.inject.Named;
import fit5042.assignment.repository.entities.Customer;

import javax.el.ELContext;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

@Named(value = "customerApplication")
@ApplicationScoped

public class CustomerApplication {
	@ManagedProperty(value = "#{customerManagedBean}")
	CustomerManagedBean customerManagedBean;
	
	private ArrayList<Customer> customers;
	
	private boolean showForm = true;
	
	public boolean isShowForm() {
		return showForm;
	}
	
	public CustomerApplication() throws Exception {
		customers = new ArrayList<>();
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
        .getELResolver().getValue(elContext, null, "customerManagedBean");
        updateCustomerList();
	}

	public ArrayList<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}
	
	public void updateCustomerList()
	{
		if (customers != null && customers.size() > 0)
		{
			
		}
		else
		{
			customers.clear();
			
			for (fit5042.assignment.repository.entities.Customer customer : customerManagedBean.getAllCustomers())
			{
				customers.add(customer);
			}
			
			setCustomers(customers);
		}
	}
	
    public void searchCustomerById(int customerId)
    {
        customers.clear();
        customers.add(customerManagedBean.searchCustomerById(customerId));
    }
    
    public void searchAll()
    {
    	customers.clear();
    	
    	for (fit5042.assignment.repository.entities.Customer customer : customerManagedBean.getAllCustomers())
    	{
    		customers.add(customer);
    	}
    	setCustomers(customers);
    }
    
    public void searchCustomerByRate(double rate)
    {
    	customers.clear();
    	
    	for(fit5042.assignment.repository.entities.Customer customer : customerManagedBean.searchCustomerByRate(rate))
    	{
    		customers.add(customer);
    	}
    	setCustomers(customers);
    }
    
}
