package fit5042.assignment.controllers;

import java.util.ArrayList;
import javax.enterprise.context.ApplicationScoped;

import fit5042.assignment.mbeans.CustomerContactManagedBean;
import fit5042.assignment.mbeans.CustomerManagedBean;
import javax.inject.Named;
import fit5042.assignment.repository.entities.Customer;

import javax.el.ELContext;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

@Named(value = "customerContactApplication")
@ApplicationScoped

public class CustomerContactApplication {
	
	@ManagedProperty(value = "#{customerContactManagedBean}")
	CustomerContactManagedBean customerContactManagedBean;
	
	private ArrayList<fit5042.assignment.repository.entities.CustomerContact> customerContacts;
	
	private boolean showForm = true;
	
	public boolean isShowForm() {
		return showForm;
	}
	
	public CustomerContactApplication() throws Exception {
		customerContacts = new ArrayList<>();
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerContactManagedBean = (CustomerContactManagedBean) FacesContext.getCurrentInstance().getApplication()
        .getELResolver().getValue(elContext, null, "customerContactManagedBean");
        updateCustomerContactList();
	}

	
	public ArrayList<fit5042.assignment.repository.entities.CustomerContact> getCustomerContacts() {
		return customerContacts;
	}

	public void setCustomerContacts(ArrayList<fit5042.assignment.repository.entities.CustomerContact> customerContacts) {
		this.customerContacts = customerContacts;
	}

	public void updateCustomerContactList()
	{
		if (customerContacts != null && customerContacts.size() > 0)
		{
			
		}
		else
		{
			customerContacts.clear();
			for(fit5042.assignment.repository.entities.CustomerContact customerContact : customerContactManagedBean.getAllCustomerContacts())
			{
				customerContacts.add(customerContact);
			}
			
			setCustomerContacts(customerContacts);
		}
	}
	
	public void searchCustomerContactById(int customerContactId)
	{
		customerContacts.clear();
		customerContacts.add(customerContactManagedBean.searchCustomerContactById(customerContactId));
		
	}
	
	public void searchAll()
	{
		customerContacts.clear();
		
		for (fit5042.assignment.repository.entities.CustomerContact customerContact : customerContactManagedBean.getAllCustomerContacts())
		{
			customerContacts.add(customerContact);
		}
		setCustomerContacts(customerContacts);
	}
	
	public void searchCustomerContactByCustomer(Customer customer)
	{
		customerContacts.clear();
		customerContacts.addAll(customerContactManagedBean.searchCustomerContactByCustomer(customer));
	}
}
