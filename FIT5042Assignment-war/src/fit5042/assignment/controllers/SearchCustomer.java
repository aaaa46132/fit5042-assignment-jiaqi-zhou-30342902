package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.assignment.controllers.CustomerApplication;

@RequestScoped
@Named("searchCustomer")
public class SearchCustomer {
	private Customer customer;
	CustomerApplication app;
	
	private int searchByInt;
	private double searchByDouble;

	public double getSearchByDouble() {
		return searchByDouble;
	}

	public void setSearchByDouble(double searchByDouble) {
		this.searchByDouble = searchByDouble;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public CustomerApplication getApp() {
		return app;
	}

	public void setApp(CustomerApplication app) {
		this.app = app;
	}

	public int getSearchByInt() {
		return searchByInt;
	}

	public void setSearchByInt(int searchByInt) {
		this.searchByInt = searchByInt;
	}
	
	public SearchCustomer() {
        ELContext context
        = FacesContext.getCurrentInstance().getELContext();

        app = (CustomerApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "customerApplication");

        app.updateCustomerList();
	}
	
	public void searchCustomerById(int customerId)
	{
		try
		{
			app.searchCustomerById(customerId);
		}
		catch (Exception ex)
		{
			
		}
	}
	
	public void searchAll()
	{
		try
		{
			app.searchAll();
		}
		catch (Exception ex)
		{
			
		}
	}
	
	public void searchCustomerByRate(double rate)
	{
		try
		{
			app.searchCustomerByRate(rate);
		}
		catch(Exception ex)
		{
			
		}
	}
	
}
