package fit5042.assignment.controllers;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import fit5042.assignment.repository.entities.Address;
import fit5042.assignment.repository.entities.Customer;
import fit5042.assignment.repository.entities.Users;
@RequestScoped
@Named(value = "customerContact")
public class CustomerContact {
	private int customerContactId;
	private String phoneNumber;
	private String email;
	private String companyName;
	private String smsNumber;
	private HashSet<String> tags;
	private Customer customer;
	
	private int customerId;
	private String industryType;
	private Date dob;
	private String name;
	private String gender;
	private Users users;
	private Address address;
	private double rate;
	private Set<fit5042.assignment.repository.entities.CustomerContact> customerContactlists;
	
	private Set<fit5042.assignment.repository.entities.CustomerContact> customerContacts;
	public CustomerContact() {
		this.customerContactlists = new HashSet<>();
		this.tags = new HashSet<>();
	}
	public int getCustomerContactId() {
		return customerContactId;
	}
	public void setCustomerContactId(int customerContactId) {
		this.customerContactId = customerContactId;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getSmsNumber() {
		return smsNumber;
	}
	public void setSmsNumber(String smsNumber) {
		this.smsNumber = smsNumber;
	}
	public HashSet<String> getTags() {
		return tags;
	}
	public void setTags(HashSet<String> tags) {
		this.tags = tags;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public String getIndustryType() {
		return industryType;
	}
	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Users getUsers() {
		return users;
	}
	public void setUsers(Users users) {
		this.users = users;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public Set<fit5042.assignment.repository.entities.CustomerContact> getCustomerContactlists() {
		return customerContactlists;
	}
	public void setCustomerContactlists(Set<fit5042.assignment.repository.entities.CustomerContact> customerContactlists) {
		this.customerContactlists = customerContactlists;
	}
	public Set<fit5042.assignment.repository.entities.CustomerContact> getCustomerContacts() {
		return customerContacts;
	}
	public void setCustomerContacts(Set<fit5042.assignment.repository.entities.CustomerContact> customerContacts) {
		this.customerContacts = customerContacts;
	}
	@Override
	public String toString() {
		return "CustomerContact [customerContactId=" + customerContactId + ", phoneNumber=" + phoneNumber + ", email="
				+ email + ", companyName=" + companyName + ", smsNumber=" + smsNumber + ", tags=" + tags + ", customer="
				+ customer + ", customerId=" + customerId + ", industryType=" + industryType + ", dob=" + dob
				+ ", name=" + name + ", gender=" + gender + ", users=" + users + ", address=" + address + ", rate="
				+ rate + ", customerContactlists=" + customerContactlists + ", customerContacts=" + customerContacts
				+ "]";
	}
	
	
}
