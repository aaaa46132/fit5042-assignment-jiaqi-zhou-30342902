package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.mbeans.CustomerContactManagedBean;
import fit5042.assignment.mbeans.CustomerManagedBean;

@RequestScoped
@Named("addCustomerContact")
public class AddCustomerContact {
	@ManagedProperty(value= "#{customerContactManagedBean}")
	CustomerContactManagedBean customerContactManagedBean;
	
	private boolean showForm = true;
	private CustomerContact customerContact;
	
	CustomerContactApplication app;
	
	public boolean isShowForm() {
		return showForm;
	}

	public CustomerContact getCustomerContact() {
		return customerContact;
	}

	public void setCustomerContact(CustomerContact customerContact) {
		this.customerContact = customerContact;
	}
	
	public AddCustomerContact()
	{
		ELContext context
        = FacesContext.getCurrentInstance().getELContext();

		app  = (CustomerContactApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "customerContactApplication");
		ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		customerContactManagedBean = (CustomerContactManagedBean) FacesContext.getCurrentInstance().getApplication()
				.getELResolver().getValue(elContext, null, "customerContactManagedBean");
	}
	
	public void addCustomerContact(CustomerContact localCustomerContact)
	{
		try {
			customerContactManagedBean.addCustomerContact(localCustomerContact);
			
			app.searchAll();
			
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer Contact has been added succesfully"));
		}
		catch (Exception ex)
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer Contact has been added failed"));
		}
		showForm = true;
	}
}
