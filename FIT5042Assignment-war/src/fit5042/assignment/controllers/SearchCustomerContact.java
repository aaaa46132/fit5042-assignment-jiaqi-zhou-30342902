package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.assignment.controllers.CustomerApplication;

@RequestScoped
@Named("searchCustomerContact")
public class SearchCustomerContact {
	private CustomerContact customerContact;
	CustomerContactApplication app;
	
	private int searchByInt;

	public CustomerContact getCustomerContact() {
		return customerContact;
	}

	public void setCustomerContact(CustomerContact customerContact) {
		this.customerContact = customerContact;
	}

	public CustomerContactApplication getApp() {
		return app;
	}

	public void setApp(CustomerContactApplication app) {
		this.app = app;
	}

	public int getSearchByInt() {
		return searchByInt;
	}

	public void setSearchByInt(int searchByInt) {
		this.searchByInt = searchByInt;
	}
	
	public SearchCustomerContact() {
        ELContext context
        = FacesContext.getCurrentInstance().getELContext();

        app = (CustomerContactApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "customerContactApplication");

        app.updateCustomerContactList();
	}
	
	public void searchCustomerContactById(int customerContactId)
	{
		try
		{
			app.searchCustomerContactById(customerContactId);
		}
		catch(Exception ex)
		{
			
		}
	}
	
	public void searchAll()
	{
		try
		{
			app.searchAll();
		}
		catch (Exception ex)
		{
			
		}
	}
	
	public void searchCustomerContactByCustomer(fit5042.assignment.repository.entities.Customer customer)
	{
		try {
			app.searchCustomerContactByCustomer(customer);
		}
		catch (Exception ex)
		{
			
		}
	}
	
}
