package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named(value = "customerController")
@RequestScoped
public class CustomerController {
	private int customerId;

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	private fit5042.assignment.repository.entities.Customer customer;
	
	public CustomerController() {
		customerId = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("customerId"));
		customer = getCustomer();
	}
	
	public fit5042.assignment.repository.entities.Customer getCustomer(){
		if(customer == null)
		{
			 ELContext context
             = FacesContext.getCurrentInstance().getELContext();
			 CustomerApplication app
             = (CustomerApplication) FacesContext.getCurrentInstance()
                     .getApplication()
                     .getELResolver()
                     .getValue(context, null, "customerApplication");
			 return app.getCustomers().get(--customerId);
		}
	return customer;
	}
}
