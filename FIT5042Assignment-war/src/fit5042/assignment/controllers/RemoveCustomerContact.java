package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.mbeans.CustomerContactManagedBean;
import fit5042.assignment.mbeans.CustomerManagedBean;
import fit5042.assignment.controllers.CustomerApplication;

import javax.faces.bean.ManagedProperty;

@RequestScoped
@Named("removeCustomerContact")
public class RemoveCustomerContact {
	@ManagedProperty(value="#{customerConttactMangedBean}")
	CustomerContactManagedBean customerContactManagedBean;
	
	private boolean showForm = true;
	private CustomerContact customerContact;
	CustomerContactApplication app;
	
	public boolean isShowForm()
	{
		return showForm;
	}

	public CustomerContact getCustomerContact() {
		return customerContact;
	}

	public void setCustomerContact(CustomerContact customerContact) {
		this.customerContact = customerContact;
	}
	
	public RemoveCustomerContact() {
        ELContext context
        = FacesContext.getCurrentInstance().getELContext();

        app = (CustomerContactApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "customerContactApplication");

        app.updateCustomerContactList();
        
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerContactManagedBean = (CustomerContactManagedBean) FacesContext.getCurrentInstance().getApplication()
        .getELResolver().getValue(elContext, null, "customerContactManagedBean");
	}
	
	public void removeCustomerContact(int customerContactId) {
		try {
			customerContactManagedBean.removeCustomerContact(customerContactId);
			app.searchAll();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer Contact has been deleted succesfully"));  
			
		}
		catch (Exception ex)
		{
			
		}
		showForm = true;
	}

}
