package fit5042.assignment.mbeans;

import fit5042.assignment.repository.CustomerRepository;
import fit5042.assignment.repository.entities.Customer;
import fit5042.assignment.repository.entities.Address;
import fit5042.assignment.repository.entities.CustomerContact;
import fit5042.assignment.repository.entities.Users;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

@ManagedBean(name = "customerManagedBean")
@SessionScoped
public class CustomerManagedBean implements Serializable {
	
	@EJB
	CustomerRepository customerRepository;
	public CustomerManagedBean() {
	}
	
	public List<Customer> getAllCustomers() {
		try {
			List<Customer> customers = customerRepository.getAllCustomers();
			return customers;
		} catch (Exception ex) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}
	
	public void addCustomer(Customer customer)
	{
		try {
			customerRepository.addCustomer(customer);
		} catch (Exception ex) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	public Customer searchCustomerById(int id)
	{
		try {
			return customerRepository.searchCustomerById(id);
		} catch (Exception ex) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		return null;
	}
	
	public void removeCustomer(int customerId)
	{
		try {
			customerRepository.removeCustomer(customerId);		
		} catch (Exception ex) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	public void editCustomer(Customer customer)
	{
		try {
			Address address = customer.getAddress();
			customer.setAddress(address);
			Users users = customer.getUsers();
			customer.setUsers(users);
			customerRepository.editCustomer(customer);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been updated succesfully"));
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
	}
	
	public Customer convertCustomerToEntity(fit5042.assignment.controllers.Customer localCustomer)
	{
		Customer customer = new Customer();
        String streetNumber = localCustomer.getStreetNumber();
        String streetAddress = localCustomer.getStreetAddress();
        String suburb = localCustomer.getSuburb();
        String postcode = localCustomer.getPostcode();
        String state = localCustomer.getState();
        String country = localCustomer.getCountry();
        Address address = new Address(streetNumber, streetAddress, suburb, postcode, state, country);
        customer.setAddress(address);
        customer.setCustomerContacts(localCustomer.getCustomerContacts());
        customer.setDob(localCustomer.getDob());
        customer.setGender(localCustomer.getGender());
        customer.setIndustryType(localCustomer.getIndustryType());
        customer.setName(localCustomer.getName());
        customer.setRate(localCustomer.getRate());
        customer.setCustomerId(localCustomer.getCustomerId());
        String username = localCustomer.getUsername();
        String password = localCustomer.getPassword();
        int id = localCustomer.getId();
        Users user = new fit5042.assignment.repository.entities.Users(id, username, password);
        if (user.getId() == 0)
        	user = null;
        customer.setUsers(user);
        return customer;
	}
	
	public void addCustomer(fit5042.assignment.controllers.Customer localCustomer)
	{
		Customer customer = convertCustomerToEntity(localCustomer);
		
		try {
			customerRepository.addCustomer(customer);
		} catch (Exception ex) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	public List<Customer> searchCustomerByRate(double rate)
	{
		try {
			return customerRepository.searchCustomerByRate(rate);
		}	catch (Exception ex) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		return null;
		
	}
}