package fit5042.assignment.mbeans;

import fit5042.assignment.repository.CustomerContactRepository;
import fit5042.assignment.repository.CustomerRepository;
import fit5042.assignment.repository.entities.Customer;
import fit5042.assignment.repository.entities.Address;
import fit5042.assignment.repository.entities.CustomerContact;
import fit5042.assignment.repository.entities.Users;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
@ManagedBean(name = "customerContactManagedBean")
@SessionScoped
public class CustomerContactManagedBean {
	@EJB
	CustomerContactRepository customerContactRepository;
	public CustomerContactManagedBean() {
	}
	
	public List<CustomerContact> getAllCustomerContacts() {
		try {
			List<CustomerContact> customerContacts = customerContactRepository.getAllCustomerContacts();
			return customerContacts;
		} catch (Exception ex) {
			Logger.getLogger(CustomerContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}
	
	public void addCustomerContact(CustomerContact customerContact)
	{
		try {
			customerContactRepository.addCustomerContact(customerContact);
		} catch (Exception ex) {
			Logger.getLogger(CustomerContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	public CustomerContact searchCustomerContactById(int id)
	{
		try {
			return customerContactRepository.searchCustomerContactById(id);
		} catch (Exception ex) {
			Logger.getLogger(CustomerContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		return null;
	}
	
	public void removeCustomerContact(int customerContactId)
	{
		try {
			customerContactRepository.removeCustomerContact(customerContactId);		
		} catch (Exception ex) {
			Logger.getLogger(CustomerContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	public void editCustomerContact(CustomerContact customerContact)
	{
		try {
			customerContactRepository.editCustomerContact(customerContact);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer Contact has been updated succesfully"));
        } catch (Exception ex) {
            Logger.getLogger(CustomerContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
	}
	
	public CustomerContact convertCustomerContactToEntity(fit5042.assignment.controllers.CustomerContact localCustomerContact)
	{
		CustomerContact customerContact = new CustomerContact();
        customerContact.setCompanyName(localCustomerContact.getCompanyName());
        customerContact.setCustomerContactId(localCustomerContact.getCustomerContactId());
        customerContact.setEmail(localCustomerContact.getEmail());
        customerContact.setPhoneNumber(localCustomerContact.getPhoneNumber());
        customerContact.setSmsNumber(localCustomerContact.getSmsNumber());
        customerContact.setTags(localCustomerContact.getTags());
        
        int customerId = localCustomerContact.getCustomerId();
		String industryType = localCustomerContact.getIndustryType();
		Date dob = localCustomerContact.getDob();
		String name = localCustomerContact.getName();
		String gender = localCustomerContact.getGender();
		Users users = localCustomerContact.getUsers();
		Address address = localCustomerContact.getAddress();
		double rate = localCustomerContact.getRate();   
        Customer customer = new fit5042.assignment.repository.entities.Customer(customerId, industryType, dob, name, gender, address, rate, users);
        if (customer.getCustomerId() == 0)
        	customer = null;
        customerContact.setCustomer(customer);
        return customerContact;
	}
	
	public void addCustomerContact(fit5042.assignment.controllers.CustomerContact localCustomerContact)
	{
		CustomerContact customerContact = convertCustomerContactToEntity(localCustomerContact);
		
		try {
			customerContactRepository.addCustomerContact(customerContact);
		} catch (Exception ex) {
			Logger.getLogger(CustomerContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
    public Set<CustomerContact> searchCustomerContactByCustomer(Customer customer) 
    {
        try {
            return customerContactRepository.searchCustomerContactByCustomer(customer);
        } catch (Exception ex) {
            Logger.getLogger(CustomerContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
}
