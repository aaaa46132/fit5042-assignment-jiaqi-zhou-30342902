package fit5042.assignment.repository;

import java.util.List;

import javax.ejb.Remote;

import fit5042.assignment.repository.entities.Customer;
import fit5042.assignment.repository.entities.CustomerContact;
@Remote
public interface CustomerRepository {
	 public void addCustomer(Customer customer) throws Exception;
	 public Customer searchCustomerById(int customerId) throws Exception;
	 public List<Customer> getAllCustomers() throws Exception;
	 public List<CustomerContact> getAllCustomerContact() throws Exception;
	 public void removeCustomer(int customerId) throws Exception;
	 public void editCustomer(Customer customer) throws Exception;
	 public List<Customer> searchCustomerByRate (double rate) throws Exception;
}
