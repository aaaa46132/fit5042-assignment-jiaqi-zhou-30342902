package fit5042.assignment.repository.entities;

import java.util.Set;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name = "Customer_Contact")
@NamedQueries({@NamedQuery(name = CustomerContact.GET_ALL_QUERY_NAME, query = "SELECT a FROM CustomerContact a order by a.customerContactId desc")})
public class CustomerContact implements Serializable {
	
    public static final String GET_ALL_QUERY_NAME = "CustomerContact.getAll";
	
	private int customerContactId;
	private String phoneNumber;
	private String email;
	private String companyName;
	private String smsNumber;
	private Set<String> tags;
	private Customer customer;
	public CustomerContact() {
		this.tags = new HashSet<>();
	}
	public CustomerContact(int customerContactId, String phoneNumber, String email, String companyName,
			Set<String> tags, Customer customer, String smsNumber) {
		this.customerContactId = customerContactId;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.companyName = companyName;
		this.tags = tags;
		this.customer = customer;
		this.smsNumber = smsNumber;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "customer_contact_id")
	public int getCustomerContactId() {
		return customerContactId;
	}
	public void setCustomerContactId(int customerContactId) {
		this.customerContactId = customerContactId;
	}
	
	@Column(name = "phone_number")
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	@Column(name = "email")
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name = "company_name")
	public String getCompanyName() {
		return companyName;
	}
	
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	@Column(name = "sms_number")
	public String getSmsNumber() {
		return smsNumber;
	}
	public void setSmsNumber(String smsNumber) {
		this.smsNumber = smsNumber;
	}
	@ElementCollection
	@CollectionTable(name = "tag")
	@Column(name = "value")
	public Set<String> getTags() {
		return tags;
	}
	public void setTags(Set<String> tags) {
		this.tags = tags;
	}
	@ManyToOne
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	@Override
	public String toString() {
		return "CustomerContact [customerContactId=" + customerContactId + ", customerId="
				+ ", phoneNumber=" + phoneNumber + ", email=" + email + ", companyName=" + companyName + ", tags="
				+ tags + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((companyName == null) ? 0 : companyName.hashCode());
		result = prime * result + customerContactId;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomerContact other = (CustomerContact) obj;
		if (companyName == null) {
			if (other.companyName != null)
				return false;
		} else if (!companyName.equals(other.companyName))
			return false;
		if (customerContactId != other.customerContactId)
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		return true;
	}
	
	
}
